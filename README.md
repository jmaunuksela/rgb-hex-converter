# RGB-HEX-Converter
This project consists of a Node.js Express app and related source files for unit and integration tests. The app is used to convert between hexadecimal and decimal representations of 3-component (red, green and blue) color values.

The web app has 3 endpoints:
- Welcome endpoint (GET)
- RGB-to-HEX endpoint (GET)
- HEX-to-RGB endpoint (GET)

## Getting Started
### 1. Clone the source code
Clone the source code with HTTPS:
```sh
git clone https://gitlab.com/jmaunuksela/rgb-hex-converter.git
```
Or with SSH:
```sh
git clone git@gitlab.com:jmaunuksela/rgb-hex-converter.git
```
### 2. Install the dependencies
```sh
npm i
```
### 3. Start the app
Start the app in production mode:
```sh
npm start
```
Or start the service in developer mode watching source code changes:
```sh
npm run dev
```
Or to run the full test suite:
```sh
npm test
```

## Usage and web API documentation
## Welcome endpoint (GET)
The endpoint URL is http://localhost:3000/api/v1. The response body contains a welcome text.

### RGB-to-HEX endpoint (GET)
The endpoint URL is http://localhost:3000/api/v1/rgb-to-hex and the query string parameters are:
- red: integer value between 0 and 255.
- green: integer value between 0 and 255.
- blue: integer value between 0 and 255.

For example: http://localhost:3000/api/v1/rgb-to-hex?red=100&green=0&blue=25. The response body contains the corresponding color value in its hexadecimal form.

### HEX-to-RGB endpoint (GET)
The endpoint URL is http://localhost:3000/api/v1/hex-to-rgb and the query string parameter is named hex. The value must be 7 characters long and must have the form:
- #xxxxxx
where the hash symbol (#) must be URL-encoded as %23 and x is a hexadecimal digit.

For example: http://localhost:3000/api/v1/hex-to-rgb?hex=%2300FFAA. The response body contains the corresponding decimal values for red, green and blue components.