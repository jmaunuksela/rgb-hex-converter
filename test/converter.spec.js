import { describe, it } from "mocha";
import { expect } from "chai";
import { rgb_to_hex, hex_to_rgb } from "../src/converter.js";

describe("RGB-to-HEX Converter", () => {
    it("is a function", () => {
        expect(rgb_to_hex).to.be.a('function');
    });
    it("should return a string", () => {
        expect(rgb_to_hex(0, 0, 0)).to.be.a('string');
    });
    it("first character is a hashtag", () => {
        expect(rgb_to_hex(0, 0, 0)[0]).to.equal("#");
    });
    it("should convert RED value correctly", () => {
        expect(rgb_to_hex(0,   0, 0).substring(0, 3)).to.equal    ("#00");
        expect(rgb_to_hex(255, 0, 0).substring(0, 3)).to.equal    ("#ff");
        expect(rgb_to_hex(136, 0, 0).substring(0, 3)).to.equal    ("#88");
        expect(rgb_to_hex(100, 0, 0).substring(0, 3)).to.not.equal("#12");
    });
    it("should convert GREEN value correctly", () => {
        expect(rgb_to_hex(0, 0,   0).substring(3, 5)).to.equal    ("00");
        expect(rgb_to_hex(0, 255, 0).substring(3, 5)).to.equal    ("ff");
        expect(rgb_to_hex(0, 136, 0).substring(3, 5)).to.equal    ("88");
        expect(rgb_to_hex(0, 100, 0).substring(3, 5)).to.not.equal("12");
    });
    it("should convert BLUE value correctly", () => {
        expect(rgb_to_hex(0, 0, 0  ).substring(5, 7)).to.equal    ("00");
        expect(rgb_to_hex(0, 0, 255).substring(5, 7)).to.equal    ("ff");
        expect(rgb_to_hex(0, 0, 136).substring(5, 7)).to.equal    ("88");
        expect(rgb_to_hex(0, 0, 100).substring(5, 7)).to.not.equal("12");
    });
    it("should convert RGB-to-HEX correctly", () => {
        expect(rgb_to_hex(255, 0,   0  )).to.equal("#ff0000");
        expect(rgb_to_hex(0,   255, 0  )).to.equal("#00ff00");
        expect(rgb_to_hex(0,   0,   255)).to.equal("#0000ff");
        expect(rgb_to_hex(255, 136, 0  )).to.equal("#ff8800");
    });
});

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.a('function');
    });
    it("should return a color object with 3 integer members", () => {
        let colorValue = hex_to_rgb("#ff00aa");

        expect(colorValue).to.have.property('red');
        expect(colorValue.red).to.be.a('number');

        expect(colorValue).to.have.property('green');
        expect(colorValue.green).to.be.a('number');

        expect(colorValue).to.have.property('blue');
        expect(colorValue.blue).to.be.a('number');
    });
    it("should should convert RED value correctly", () => {
        expect(hex_to_rgb("#000000").red).to.equal(0);
        expect(hex_to_rgb("#ff0000").red).to.equal(255);
        expect(hex_to_rgb("#880000").red).to.equal(136);
        expect(hex_to_rgb("#120000").red).to.not.equal(100);
    });
    it("should should convert GREEN value correctly", () => {
        expect(hex_to_rgb("#000000").green).to.equal(0);
        expect(hex_to_rgb("#00ff00").green).to.equal(255);
        expect(hex_to_rgb("#008800").green).to.equal(136);
        expect(hex_to_rgb("#001200").green).to.not.equal(100);
    });
    it("should should convert BLUE value correctly", () => {
        expect(hex_to_rgb("#000000").blue).to.equal(0);
        expect(hex_to_rgb("#0000ff").blue).to.equal(255);
        expect(hex_to_rgb("#000088").blue).to.equal(136);
        expect(hex_to_rgb("#000012").blue).to.not.equal(100);
    });
    it("should convert HEX-to-RGB correctly", () => {
        expect(hex_to_rgb("#ff0000")).to.deep.equal({red: 255, green: 0, blue: 0});
        expect(hex_to_rgb("#00ff00")).to.deep.equal({red: 0, green: 255, blue: 0});
        expect(hex_to_rgb("#0000ff")).to.deep.equal({red: 0, green: 0, blue: 255});
        expect(hex_to_rgb("#ff8800")).to.deep.equal({red: 255, green: 136, blue: 0});
    });
});