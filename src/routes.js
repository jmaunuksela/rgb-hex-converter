import { Router } from 'express';
import { rgb_to_hex, hex_to_rgb } from './converter.js';

const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome!"));

// Endpoint GET /rgb-to-hex?red=255&green=136&blue=0
routes.get('/rgb-to-hex', (req, res) => {
    const RED = parseFloat(req.query.red);
    const GREEN = parseFloat(req.query.green);
    const BLUE = parseFloat(req.query.blue);
    const HEX = rgb_to_hex(RED, GREEN, BLUE);   // integraatio
    res.status(200).send(HEX);
});

// Endpoint GET /hex-to-rgb?hex=#ff8800 -> URL encoded: /hex-to-rgb?hex=%23ff8800
routes.get('/hex-to-rgb', (req, res) => {
    const HEX = req.query.hex;
    const COLORS = hex_to_rgb(HEX);
    if (HEX[0] != '#' || HEX.length != 7 || isNaN(COLORS.red) || isNaN(COLORS.green) || isNaN(COLORS.blue)
    || HEX.red < 0 || HEX.green < 0 || HEX.blue < 0)
        res.status(400).send("Hex color value was not in the correct format!" +
        " Note: Remember to type the prefix (#) in the query string in its URL encoded form (as %23)! Individual color values must be 2 digits and in the range 00-FF.");
    else
        res.status(200).send(`red: ${COLORS.red}, green: ${COLORS.green}, blue: ${COLORS.blue}`);
});

export default routes;