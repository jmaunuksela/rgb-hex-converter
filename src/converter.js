/**
 * Padding hex component if necessary.
 * Hex component representation requires
 * two hexadecimal characters.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" + comp;
    return padded;
};

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * Converts a string representation of a hex color value
 * into an object with 3 integer members (red, green and blue). The
 * input string must be in the form "#xxxxxx", where x is a hex
 * digit.
 * @param {string} hex in hex color format, e.g., "#00ff00" (green)
 * @returns {[number, number, number]} red, green and blue colors (0-255)
 */
export const hex_to_rgb = (hex) => {
    const RED = parseInt(hex.substring(1, 3), 16);
    const GREEN = parseInt(hex.substring(3, 5), 16);
    const BLUE = parseInt(hex.substring(5, 7), 16);
    return {red: RED, green: GREEN, blue: BLUE};
};