import express from 'express';
import routes from './routes.js';

const api_url = "http://localhost:3000/api/v1";
const app = express();
app.use("/api/v1", routes);
app.listen(3000, () => console.log(`Server listening at ${api_url}`));